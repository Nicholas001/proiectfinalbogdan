package com.sda.auction.dto;

import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
@ToString
public class UserLoginForm {

    @Email(message = "Must be a valid email")
    @NotEmpty(message = "Email should not be empty")
    private String email;

    @Length(min = 4, message = "Password too short")
    private String password;
    private String confirmPassword;

}
